import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

def plot_t_distribution(degrees_of_freedom,tt,tc):
    # Generate x-values
    x = np.linspace(-15, 15, 1000)

    # Calculate t-distribution values
    t_distribution = stats.t.pdf(x, degrees_of_freedom)
    # Plot the t-distribution
    plt.plot(x, t_distribution)
    plt.axvline(tt, color='r', linestyle='--', label='t_tablas = '+ str(tt))
    plt.axvline(tc, color='g', linestyle='--', label='t_calculada ='+ str(tc))
    #fill left side graph area from tt value of purple color
    plt.fill_between(x, 0, t_distribution, where=(x >= tt), color='purple', alpha=0.5)
    #label "Region de no rechazo" for purple area in label
    plt.text(-1, 0.1, 'Región de no rechazo',color='purple')
    plt.xlabel('t')
    plt.ylabel('Densidad de probabilidad')
    plt.title('Distribución-t muestra')
    plt.legend()
    plt.grid(True)
    #save the plot in full resolution
    plt.show()

# Example usage: Plot t-distribution with 10 degrees of freedom
tt = -1.8125
tc= -14.7329
n = 11
plot_t_distribution(n-1,tt,tc)