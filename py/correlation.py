import numpy as np
import matplotlib.pyplot as plt

def leer_csv(nombre):
    import csv
    with open(nombre) as archivo:
        lector = csv.reader(archivo)
        lista = []
        for linea in lector:
            lista.append(float(linea[0]))
    return lista

def media(lista):
    return sum(lista)/len(lista)

def dispersionGraph(data,data2):
    plt.scatter(data, data2)
    plt.xlabel('Espesor de M2 (g/mm²)')
    plt.ylabel('Peso de M2 (g)')
    plt.title('Gráfica de dispersión')
    #add tendency line
    m, b = np.polyfit(data, data2, 1)
    plt.plot(data, m*np.array(data) + b, color='red')
    plt.grid(True)
    plt.savefig('dispersion2.png')
    plt.show()

def sxy(x, y):
    n = len(x)
    return sum([(x[i] - media(x))*(y[i] - media(y)) for i in range(n)])

def sxx(x):
    n = len(x)
    return sum([(i - media(x))**2 for i in x])

def syy(y):
    n = len(y)
    return sum([(i - media(y))**2 for i in y])

def r(data, data2):
    return sxy(data, data2)/((sxx(data)*syy(data2))**0.5)

data = leer_csv('lista3.1.csv')
data2 = leer_csv('lista4.1.csv')
data3 = leer_csv('lista3.2.csv')
data4 = leer_csv('lista4.2.csv')
print("Correlación entre espessor y peso para M1")
print("Sxy= "+ str(sxy(data, data2)))
print("Sxx= "+ str(sxx(data)))
print("Syy= "+ str(syy(data2)))
print("r= "+ str(r(data, data2)))
print("Correlación entre espessor y peso para M2")
print("Sxy= "+ str(sxy(data3, data4)))
print("Sxx= "+ str(sxx(data3)))
print("Syy= "+ str(syy(data4)))
print("r= "+ str(r(data3, data4)))


#dispersionGraph(data2, data)
dispersionGraph(data4, data3)

# Correlación entre espessor y peso para M1
# Sxy= 0.24150000000000008
# Sxx= 0.07040000000000003
# Syy= 0.9806909090909092
# r= 0.9191041578885082
# Correlación entre espessor y peso para M2
# Sxy= 0.12721818181818179
# Sxx= 0.02585454545454544
# Syy= 0.6408727272727273
# r= 0.9883137460077085