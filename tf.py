import numpy as np
import seaborn as sns
import csv

import matplotlib.pyplot as plt

# Función para leer datos de un archivo CSV
def leer_csv(nombre):
    with open(nombre) as archivo:
        lector = csv.reader(archivo)
        lista = [float(linea[0]) for linea in lector]
    return lista

# Función para crear la tabla de frecuencia y guardarla como un archivo CSV
def crear_tabla_frecuencia(data, bin_edges, output_file):
    abs_freq = np.histogram(data, bins=bin_edges)[0]
    abs_freq_acum = np.cumsum(abs_freq)
    rel_freq = abs_freq / len(data)
    rel_freq_acum = np.cumsum(rel_freq)
    
    with open(output_file, 'w', newline='') as archivo:
        escritor = csv.writer(archivo)
        escritor.writerow(['# Clase', 'Rango de Clase', 'Frecuencia Absoluta', 'Frecuencia Absoluta Acumulada', 'Frecuencia Relativa', 'Frecuencia Relativa Acumulada'])
        
        for i in range(len(abs_freq)):
            clase = i + 1
            rango_clase = f'[{bin_edges[i]:.2f}-{bin_edges[i+1]:.2f}]'
            freq_abs = abs_freq[i]
            freq_abs_acum = abs_freq_acum[i]
            freq_rel = rel_freq[i]
            freq_rel_acum = rel_freq_acum[i]
            escritor.writerow([clase, rango_clase, freq_abs, freq_abs_acum, freq_rel, freq_rel_acum])

# Read data from CSV file
data = leer_csv('lista4.2.csv')

# Calculate the number of bins using Sturges' rule
N = len(data)
sturges_bins = int(np.ceil(np.log2(N) + 1))
print(sturges_bins)

# Calculate the bin width
data_min, data_max = min(data), max(data)
data_range = data_max - data_min
bin_width = data_range / sturges_bins
print(bin_width)

# Create bin edges
bin_edges = [data_min + (i * bin_width) for i in range(sturges_bins + 1)]

# Create and save the frequency table
crear_tabla_frecuencia(data, bin_edges, 'f4.2.csv')
