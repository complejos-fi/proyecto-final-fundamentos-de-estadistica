#para calcular medias
def media(lista):
    return sum(lista)/len(lista)

#para calcular varianza
def varianza(lista):
    promedio = media(lista)
    sumatoria = 0
    for i in lista:
        sumatoria += (i-promedio)**2
    return (sumatoria/(len(lista)-1))

#para calcular desviacion estandar
def desviacion(lista):
    return varianza(lista)**0.5

#funcion que lea la lista de un csv
def leer_csv(nombre):
    import csv
    with open(nombre) as archivo:
        lector = csv.reader(archivo)
        lista = []
        for linea in lector:
            lista.append(float(linea[0]))
    return lista

#funcion que escriba una lista que ingrese
def escribir_csv(nombre, lista):
    import csv
    with open(nombre, 'w') as archivo:
        escritor = csv.writer(archivo)
        for i in lista:
            escritor.writerow([i])

#funcion que lee una lista
def leer_lista():
    lista = []
    while True:
        i = input("Ingrese un numero o 'fin' para terminar: ")
        if(i == 'fin'):
            return lista
        else:
            lista.append(float(i))

l11 =[]
l12 =[]
l21 =[]
l22 =[]
l31 =[]
l32 =[]
l41 =[]
l42 =[]

l11 = leer_csv('lista1.1.csv')
l12 = leer_csv('lista1.2.csv')
l21 = leer_csv('lista2.1.csv')
l22 = leer_csv('lista2.2.csv')
l31 = leer_csv('lista3.1.csv')
l32 = leer_csv('lista3.2.csv')
l41 = leer_csv('lista4.1.csv')
l42 = leer_csv('lista4.2.csv')
lm = [media(l11), media(l12), media(l21), media(l22), media(l31), media(l32), media(l41), media(l42)]
lv = [varianza(l11), varianza(l12), varianza(l21), varianza(l22), varianza(l31), varianza(l32), varianza(l41), varianza(l42)]
ld = [desviacion(l11), desviacion(l12), desviacion(l21), desviacion(l22), desviacion(l31), desviacion(l32), desviacion(l41), desviacion(l42)]

'''
escribir un csv con el siguiente formato:
Tabla1.1
m=
v=
d=
Tabla1.2
m=
v=
d=
'''
i = 0
j = 1
s = []
while i < len(lm):
    s.append('tabla'+str(j)+'.1: ')
    s.append('x:'+ str(lm[i]))
    s.append('S²:'+str(lv[i]))
    s.append('S:'+str(ld[i]))
    s.append('tabla'+str(j)+'.2: ')
    s.append('x:'+ str(lm[i+1]))
    s.append('S²:'+str(lv[i+1]))
    s.append('S:'+str(ld[i+1]))
    i += 2
    j += 1

escribir_csv('resultados.csv', s)
