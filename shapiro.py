import numpy as np
from scipy.stats import shapiro

def leer_csv(nombre):
    import csv
    with open(nombre) as archivo:
        lector = csv.reader(archivo)
        lista = []
        for linea in lector:
            lista.append(float(linea[0]))
    return lista

data = leer_csv('lista1.1.csv')

stat, p_value = shapiro(data)
print(f'Statistics={stat}, p-value={p_value}')

alpha = 0.05
if p_value > alpha:
    print('Normal dist.(No se rechaza H0)')
else:
    print('non-Normal dist. (Se rechaza H0)')
