import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Generar datos de ejemplo
def leer_csv(nombre):
    import csv
    with open(nombre) as archivo:
        lector = csv.reader(archivo)
        lista = []
        for linea in lector:
            lista.append(float(linea[0]))
    return lista

data = leer_csv('lista4.2.csv')

# Calcular el número de intervalos usando la Regla de Sturges
N = len(data)
sturges_bins = int(np.ceil(np.log2(N) + 1))
print(sturges_bins)

# Calcular el tamaño del intervalo (h)
data_min, data_max = min(data), max(data)
data_range = data_max - data_min
bin_width = data_range / sturges_bins
print(bin_width)

# Crear los límites de los intervalos
bin_edges = [data_min + (i * bin_width) for i in range(sturges_bins + 1)]

# Graficar el histograma
plt.figure(figsize=(10, 6))
sns.histplot(data, bins=bin_edges, kde=False)
plt.title(f'Histograma {sturges_bins} intervalos, para Peso de M2')
plt.xlabel('Peso (g)')
plt.ylabel('Frecuencia')
plt.grid(True)
plt.savefig('hist4.2.png')
plt.show()